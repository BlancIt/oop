<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<title>Index</title>
</head>
<body>
<?php

require_once('animal.php');
require_once('frog.php');
require_once('ape.php');

echo "<h3>Release 0</h3>";

$sheep = new Animal("shaun");

echo $sheep->name . "<br>"; // "shaun"
echo $sheep->legs . "<br>"; // 4
echo $sheep->cold_blooded . "<br>"; // "no"

echo "<h3>Release 1</h3>";

echo "<h4>Sungokong</h4>";
$sungokong = new Ape("kera sakti");
echo $sungokong->name . "<br>";
echo $sungokong->legs . "<br>";
echo $sungokong->cold_blooded . "<br>";
echo $sungokong->yell() . "<br>"; // "Auooo"

echo "<h4>Kodok</h4>";

$kodok = new Frog("buduk");
echo $kodok->name . "<br>";
echo $kodok->legs . "<br>";
echo $kodok->cold_blooded . "<br>";
echo $kodok->jump() . "<br>"; // "hop hop"

?>

</body>

</html>